# Init project
```sh
go mod init gitlab.com/csdaomin/go-movie
mkdir -p bin cmd/api internal migrations remote
touch Makefile
touch  cmd/api/main.go
```

├── bin   包含编译后的可执行文件
├── cmd   
│   └── api   包含应用的主程序（应用入口）
│       └── main.go
├── doc
│   ├── REQUIREMENT.md
│   └── tutorials.md
├── go.mod
├── internal
├── Makefile
├── migrations
├── README.md
└── remote