package data

import (
	"fmt"
	"strconv"
)

type Runtime int32

func (r Runtime) MarshalJSON() ([]byte, error) {
	jsonValue := fmt.Sprintf("%d mins", r)
	quotedJSONValue := strconv.Quote(jsonValue)

	return []byte(quotedJSONValue), nil
}

func (r *Runtime) UnmarshalJSON(b []byte) error {
	unquotedJSONValue := string(b)
	unquotedJSONValue = unquotedJSONValue[1 : len(unquotedJSONValue)-1]

	parsedValue, err := strconv.Atoi(unquotedJSONValue)
	if err != nil {
		return err
	}

	*r = Runtime(parsedValue)

	return nil
}
