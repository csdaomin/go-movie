package data

import (
	"time"
)

type Movie struct {
	ID        int64     `json:"id"`
	CreatedAt time.Time `json:"-"`
	Title     string    `json:"title"`
	Year      int32     `json:"year,omitempty"`
	Runtime   Runtime   `json:"runtime,omitempty,string"`
	Genres    []string  `json:"genres,omitempty"`
	// The version number starts at 1
	// and will be incremented every time the movie info is updated
	Version int32 `json:"version"`
}

// Implement a MarshalJSON method for the Movie struct,
// so that the runtime.MarshalJSON() function can be used
// func (m Movie) MarshalJSON() ([]byte, error) {
// 	var runtime string

// 	if m.Runtime != 0 {
// 		runtime = fmt.Sprintf("%d mins", m.Runtime)
// 	}

// 	aux := struct {
// 		ID      int64    `json:"id"`
// 		Title   string   `json:"title"`
// 		Year    int32    `json:"year,omitempty"`
// 		Runtime string   `json:"runtime,omitempty"`
// 		Genres  []string `json:"genres,omitempty"`
// 		Version int32    `json:"version"`
// 	}{
// 		ID:      m.ID,
// 		Title:   m.Title,
// 		Year:    m.Year,
// 		Runtime: runtime,
// 		Genres:  m.Genres,
// 		Version: m.Version,
// 	}

// 	return json.Marshal(aux)
// }
