package data

import (
	"time"

	"gitlab.com/csdaomin/go-movie/internal/validator"
)

type Movie struct {
	ID        int64     `json:"id"`
	CreatedAt time.Time `json:"-"`
	Title     string    `json:"title"`
	Year      int32     `json:"year,omitempty"`
	Runtime   Runtime   `json:"runtime,omitempty"`
	Genres    []string  `json:"genres,omitempty"`
	// The version number starts at 1
	// and will be incremented every time the movie info is updated
	Version int32 `json:"version"`
}

func ValidateMovie(v *validator.Validator, input *data.Movie) {
	v.Check(input.Title != "", "title", "must be provided")
	v.Check(len(input.Title) <= 200, "title", "must not be more than 200 bytes long")

	v.Check(input.Year != 0, "year", "must be provided")
	v.Check(input.Year >= 1900 && int(input.Year) <= time.Now().Year(), "year", "must be between 1900 and the current year")

	v.Check(input.Runtime != 0, "runtime", "must be provided")
	v.Check(input.Runtime > 0, "runtime", "must be greater than 0")

	v.Check(input.Genres != nil && len(input.Genres) > 0, "genres", "must be provided")
	v.Check(len(input.Genres) <= 10, "genres", "must not be more than 10 items")
	v.Check(validator.Unique(input.Genres), "genres", "must not contain duplicate values")
}
