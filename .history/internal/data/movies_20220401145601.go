package data

type Movie struct {
	ID        int64    `json:"id"`
	CreatedAt string   `json:"created_at"`
	Title     string   `json:"title"`
	Year      int32    `json:"year"`
	Runtime   int32    `json:"runtime"`
	Genres    []string `json:"genres"`
	// The version number starts at 1
	// and will be incremented every time the movie info is updated
	Version int32 `json:"version"`
}
