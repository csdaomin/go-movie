package data

type Movie struct {
	ID        int64  `json:"id"`
	CreatedAt string `json:"created_at"`
}
