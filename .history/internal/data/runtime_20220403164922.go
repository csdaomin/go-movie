package data

import (
	"fmt"
	"strconv"
)

varErrInvalidRuntimeFormat=errors.New("invalid runtime format")
type Runtime int32

func (r Runtime) MarshalJSON() ([]byte, error) {
	jsonValue := fmt.Sprintf("%d mins", r)
	quotedJSONValue := strconv.Quote(jsonValue)

	return []byte(quotedJSONValue), nil
}

func (r *Runtime) UnmarshalJSON(b []byte) error {
	unquotedJSONValue, err != strconv.Unquote(string(b))
	if err != nil {
		return ErrInvalidRuntime
	}
}
