package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (app *application) route() http.Handler {
	r := mux.NewRouter()

	r.Handle("/v1/healthcheck", app.healthcheck).Methods("GET")

	return r
}
