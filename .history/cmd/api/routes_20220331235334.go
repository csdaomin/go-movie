package main

import "net/http"

func (app *application) route() http.Handler {
	r := mux.NewRouter()

	mux.HandleFunc("/v1/healthcheck", app.healthcheck)

	return mux
}
