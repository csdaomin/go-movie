func (app *application) logError(r *http.Request, err error) {
	if err != nil {
		app.logger.Printf("[ERROR] %s: %s\n", r.Method, r.URL.Path)
		app.logger.Println(err)
	}
}