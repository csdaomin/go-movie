package main

import (
	"fmt"
	"net/http"
)

func (app *application) healthcheck(w http.ResponseWriter, r *http.Request) {
	json := `{"status": "ok", "version": "1.0.0"， "env": %q }`
	json = fmt.Sprintf(json, app.config.env)
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(json))
}

func (app *application) createMovie(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "status: ok")
}

func (app *application) showMovie(w http.ResponseWriter, r *http.Request) {
	id, err := app.readIDParam(r)
	if err != nil || id < 1 {
		http.NotFound(w, r)
		return
	}
	fmt.Fprintln(w, "status: ok")
}
