package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (app *application) route() http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/v1/healthcheck", app.healthcheck).Methods("GET")

	return r
}
