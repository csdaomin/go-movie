package main

import (
	"flag"
	"log"
	"os"
)

// Define a string containing the application version number
const version = "0.1.0"

// Define a config struct to hold all the configuration setting for our application
type config struct {
	// Define server port
	port int
	// Define environment for the application(development/staging/production, etc.)
	env string
}

// Define an application struct to hold the dependencies
// for our application
type application struct {
	config config
	logger *log.Logger
}

func main() {

	var cfg config

	flag.IntVar(&cfg.port, "port", 8081, "server port")
	flag.StringVar(&cfg.env, "env", "development", "Environment(development|staging|production)")
	flag.Parse()

	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)
	app := &application{
		config: cfg,
		logger: logger,
	}
}
