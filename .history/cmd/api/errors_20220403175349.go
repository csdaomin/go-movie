package main

import (
	"net/http"
)

// The logError method is a generic helper for logging an error message.
func (app *application) logError(r *http.Request, err error) {
	app.logger.Println(err)
}

func (app *application) badRequestResponse(w http.ResponseWriter, r *http.Request, message string) {
	app.errorResponse(w, r, http.StatusBadRequest, message)
}

// The errorResponse method is a generic helper for sending JSON formatted error message
// to the client with a given status code.
func (app *application) errorResponse(w http.ResponseWriter, r *http.Request, status int, message interface{}) {
	env := envelope{"error": message}

	err := app.writeJSON(w, status, env, nil)
	if err != nil {
		app.logError(r, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

// The serverErrorResponse method is used to return a 500 response.
func (app *application) serverErrorResponse(w http.ResponseWriter, r *http.Request, err error) {
	app.logError(r, err)

	message := "the server encountered a problem and could not process your request"
	app.errorResponse(w, r, http.StatusInternalServerError, message)
}

// The notFoundResponse method is used to return a 404 response.
func (app *application) notFoundResponse(w http.ResponseWriter, r *http.Request) {
	message := "the requested resource could not be found"
	app.errorResponse(w, r, http.StatusNotFound, message)
}

// The methodNotAllowedResponse method is used to return a 405 response.
func (app *application) methodNotAllowedResponse(w http.ResponseWriter, r *http.Request) {
	message := "the requested method is not allowed for the requested resource"
	app.errorResponse(w, r, http.StatusMethodNotAllowed, message)
}

func (app*application) failedValidationResponse(whttp.ResponseWriter,r*http.Request,errorsmap[string]string){app.errorResponse(w,r,http.StatusUnprocessableEntity,errors)
}