package main

import (
	"fmt"
	"log"
)

// Define a string containing the application version number
const version = "0.1.0"

// Define a config struct to hold all the configuration setting for our application
type config struct {
	// Define a string containing the application version number
	port int
	// Define environment for the application
	env string
}

type application struct {
	config config
	logger *log.Logger
}

func main() {
	fmt.Println("Hello, world!")
}
