package main

import "net/http"

func (app *application) route() {
	mux := http.NewServeMux()
	mux.HandleFunc("/v1/healthcheck", app.healthcheck)
}
