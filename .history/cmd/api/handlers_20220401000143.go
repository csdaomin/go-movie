package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func (app *application) healthcheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "status: ok")
	fmt.Fprintf(w, "environment: %s\n", app.config.env)
}

func (app *application) createMovie(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "status: ok")
}

func (app *application) showMovie(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fmt.Fprintln(w, "status: ok")
}
