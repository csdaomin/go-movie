package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (app *application) route() http.Handler {
	router := mux.NewRouter()

	router.NotFoundHandler = http.HandlerFunc(app.notFoundResponse)

	router.HandleFunc("/v1/healthcheck", app.healthcheck).Methods("GET")
	router.HandleFunc("/v1/movies", app.createMovie).Methods("GET")
	router.HandleFunc("/v1/movies/{id}", app.showMovie).Methods("GET")
	return router
}
