package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (app *application) route() http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/v1/healthcheck", app.healthcheck).Methods("GET")

	return r
}
