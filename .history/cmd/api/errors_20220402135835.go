package main

import "net/http"

func (app *application) logError(r *http.Request, err error) {
	app.logger.Println(err)
}

func (app *application) errorResponse(w http.Response, r *http.Request, status int, message interface{}) {
	err := app.writeJSON(w, status, envelope{
		"status":  "error",
		"message": message,
	}, nil)
	if err != nil {
		app.logError(r, err)
	}
}
