package main

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func (app *application) readIDParam(r http.Request) (int64, error) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil || id < 1 {
		return 0, err
	}
	return int64(id), nil
}
