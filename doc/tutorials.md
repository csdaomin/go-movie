# Init project
```sh
go mod init gitlab.com/csdaomin/go-movie
mkdir -p bin cmd/api internal migrations remote
touch Makefile
touch  cmd/api/main.go
```

├── bin   包含编译后的可执行文件
├── cmd   
│   └── api   包含应用的主程序（应用入口）
│       └── main.go
├── go.mod
├── internal    Go的专用目录名，只有当前项目的代码才可以访问
├── Makefile
├── migrations    包含数据库迁移文件
├── README.md
└── remote    包含配置文件和远程调用代码

